<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require_once 'models/auth.php';
require_once 'models/exams.php';
require_once 'models/results.php';
require_once 'models/jobs.php';
require_once 'models/email.php';

$app = new \Slim\App;

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->get('/hello/{name}', function (Request $request, Response $response, array $args) {
    $name = $args['name'];
    $response->getBody()->write("Hello, $name");

    return $response;
});

//Function to test email service
$app->post('/testemail', function ($request,$response) {
$email = new email();
$res = $email->sendmail('sainik18@gmail.com','Hello test', 'this is a test msg');
});

// Function to login admin
$app->post('/login', function ($request,$response) {
    $auth = new auth();
    $req = $request->getParsedBody();

    //$email = $req['email'];
    //$password = $req['password'];

    $data = $auth->checklogin($req['email'],$req['password']);

    if ($data){
        $res = array('status' => TRUE, 'data' => $data);
    }else {
        $res = array('status' => FALSE, 'msg' => 'Invalid Credentials!');
    }

    echo json_encode($res);
});

// Function to login student
$app->post('/userlogin', function ($request,$response) {
    
    $auth = new auth();
    $req = $request->getParsedBody();

    $data = $auth->checkuserlogin($req['email'],$req['password']);

    if ($data){
        $res = array('status' => TRUE, 'data' => $data);
    }else {
        $res = array('status' => FALSE, 'msg' => 'Invalid Credentials!');
    }

    echo json_encode($res);
});

//Function to get user data
$app->post('/getUserData', function ($request,$response) {
    
    $auth = new auth();
    $req = $request->getParsedBody();

    $data = $auth->getUserData($req['email']);

    if ($data){
        $res = array('status' => TRUE, 'data' => $data);
    }else {
        $res = array('status' => FALSE, 'msg' => 'Invalid Credentials!');
    }

    echo json_encode($res);
});

//Function to get user data
$app->post('/getUserEduData', function ($request,$response) {
    
    $auth = new auth();
    $req = $request->getParsedBody();

    $data = $auth->getUserEduData($req['uid']);

    if ($data){
        $res = array('status' => TRUE, 'data' => $data);
    }else {
        $res = array('status' => FALSE, 'msg' => 'No Data Found!');
    }

    echo json_encode($res);
});

//Function to update user edu data
$app->post('/updateUserEduData', function ($request,$response) {
    
    $auth = new auth();
    $req = $request->getParsedBody();

    $data = $auth->updateUserEduData($req);

    if ($data){
        $res = array('status' => TRUE, 'msg' => 'Updated Successfully!');
    }else {
        $res = array('status' => FALSE, 'msg' => 'No Data Found!');
    }

    echo json_encode($res);
});

$app->post('/updateUser', function ($request,$response) {
    
    $auth = new auth();
    $req = $request->getParsedBody();
    $data = $auth->updateUserData($req);

    if($data) {
        $res = array('status' => TRUE, 'msg' => 'Updated Successfully!');
    }else {
        $res = array('status' => FALSE, 'msg' => 'Something Went Wrong!');
    }

    echo json_encode($res);

});

// Function to register social user
$app->post('/socialregister', function ($request,$response) {
    
    $auth = new auth();
    $req = $request->getParsedBody();

    $data = $auth->checkEmailExists($req['email']);

    if ($data){
        $res = array('status' => TRUE, 'data' => $data);
    }else {
        //register User
        $reg = $auth->registerSocialUser($req['email'], $req['name'], $req['image'], $req['provider']);
        
        $dataa = $auth->checkEmailExists($req['email']);
        if ($dataa){
            $res = array('status' => TRUE, 'data' => $dataa);
        }else {
            $res = array('status' => FALSE, 'data' => $dataa);
        }

    }

    echo json_encode($res);
});

//Function to register user
$app->post('/registerUser', function ($request,$response) {
    $auth = new auth();
    $req = $request->getParsedBody();
    $userExists = $auth->checkEmailExists($req['email']);

    if($userExists){
        $res = array('status' => FALSE, 'msg' => 'Email Already Registered!');
    }else {
        $reg = $auth->registerUser($req);
        if($reg){
            $res = array('status' => TRUE, 'msg' => 'Registered Successfully, Password Sent To Your Email!');
        }else {
            $res = array('status' => FALSE, 'msg' => 'Something Went Wrong!');
        }
    }

    echo json_encode($res);

});

// Function to get exam types
$app->post('/getexamtypes', function ($request,$response) {
    $exams = new exams();
    $req = $request->getParsedBody();

    $data = $exams->getexamtypes();

    if ($data){
        $res = array('status' => TRUE, 'data' => $data);
    }else {
        $res = array('status' => FALSE, 'msg' => 'No Data Found!');
    }

    echo json_encode($res);
});

// Function to get exams
$app->post('/getexams', function ($request,$response) {
    $exams = new exams();
    $req = $request->getParsedBody();

    $data = $exams->getexams($req['course']);

    if ($data){
        $res = array('status' => TRUE, 'data' => $data);
    }else {
        $res = array('status' => FALSE, 'msg' => 'No Data Found!');
    }

    echo json_encode($res);
});


//Function to get questions by exam id
$app->post('/getQuestionsByEId', function ($request,$response) {
    $exams = new exams();
    $req = $request->getParsedBody();

    $data = $exams->getAllQuestionsByExamId($req['eid']);

    if($data){
        $res = array('status' => TRUE, 'data' => $data);
    }else {
        $res = array('status' => FALSE, 'msg' => 'No questions available for the selected exam');
    }
    echo json_encode($res);

});

//Function to review exam by token
$app->post('/reviewExamByTokenId', function ($request,$response) {
    $results = new results();
    $req = $request->getParsedBody();

    $data = $results->getReviewExamByTokenId($req['uid'], $req['token']);

    echo json_encode($data);

});

// Function to add questions
$app->post('/addquestion', function ($request,$response) {
    $exams = new exams();
    $req = $request->getParsedBody();

    // Check question if exists
    $quesExists = $exams->checkQuestionExists($req['question']);

    if($quesExists){
        $res = array('status' => FALSE, 'msg' => 'Question Already Exists!');
    }else{
        $qid = $exams->insertQuestion($req);
        $res = $exams->insertAnswersByQid($req, $qid);

        if($res){
            $res = array('status' => TRUE, 'msg' => 'Inserted Successfully!');
        }else {
            $res = array('status' => FALSE, 'msg' => 'Something Went Wrong!');
        }

    }
    
    echo json_encode($res);

});

//function to edit question 
$app->post('/editquestion', function ($request,$response) {
    $exams = new exams();
    $req = $request->getParsedBody();

    $data = $exams->editQuestionAndAnswers($req);

    if($data){
        $res = array('status' => TRUE, 'msg' => 'Updated Successfully!');
    }else {
        $res = array('status' => FALSE, 'msg' => 'Something Went Wrong!');
    }

    echo json_encode($res);
});

// Functions to get questions by id
$app->post('/getQuestionsById', function ($request,$response) {
    $exams = new exams();
    $req = $request->getParsedBody();
    $examid = $exams->getExamId($req['examid']);

    if($examid){
        $questions = $exams->getQuestionsByExamId($examid[0]->id);

        if($questions){
            $res = array('status' => TRUE, 'data' => $questions, 'examdata' => $examid);
        }else {
            $res = array('status' => FALSE, 'msg' => 'Questions Does not exists !');
        }
    }else {
        $res = array('status' => FALSE, 'msg' => 'Course Does not exists !');
    }

    echo json_encode($res);

});

// Functions to get Answers by Question id
$app->post('/getAnswersByQid', function ($request,$response) {
    $exams = new exams();
    $req = $request->getParsedBody();
    
    $answers = $exams->getAnswersbyQid($req['qid']);

    if($answers){
        $res = array('status' => TRUE, 'data' => $answers);
    }else {
        $res = array('status' => FALSE, 'msg' => 'Answers Does not exists !');
    }

    echo json_encode($res);

});

// Function to get user results by user id
$app->post('/getUserResultsByUid', function ($request,$response) {
    $results = new results();
    $req = $request->getParsedBody();
    $results = $results->getUserResults($req['userId']);

    if($results){
        $res = array('status' => TRUE, 'data' => $results);
    }else {
        $res = array('status' => FALSE, 'msg' => 'Course Does not exists !');
    }

    echo json_encode($res);

});

$app->post('/saveQsnAns', function ($request,$response) {
    $exams = new exams();
    
    $req = $request->getParsedBody();
    // generate token
    $token = time();
    
    $data = $req['data'];
    $uid = $req['uid'];
    $totalqsns = $req['totalqsns'];
    $duration = $req['duration'];
    $totalMarks = $req['totalMarks'];
    $totalCorrect = 0;
    $passmarks = 0;
    foreach($data as $v){
        $question = $exams->getQuestionByQid($v['qid']);

        $insTqsn = $exams->insertTQuestion($question[0], $uid, $token);

        //$answer = $exams->getAnswerbyId($v['ans']);
        $answer = $exams->getAnswerbyId($v['qid']);
        $i = 1;
        foreach($answer as $a){
            if($v['ans'] == $a->id){
                $marked = $i;
                $ansdata = $a;
                
            }
            if($a->correct == 1){
                $correct = $i;
            }
            $i++;
        }

        if($correct == $marked){
            $totalCorrect = $totalCorrect+1;
            $passmarks = $passmarks + $question[0]->marks;
        }
        
        $insTAns = $exams->insertTAnswer($ansdata, $uid, $insTqsn, $correct, $marked);

    }

    $totalWrong = $totalqsns - $totalCorrect;
    $pscore = round($passmarks / $totalMarks * 100 , 2);
    
    if($pscore >= 60){
        $remarks = 1;
    }else{
        $remarks = 0;
    }
    // Insert exam report result table
    $result = $exams->saveUserResult($uid, $question[0]->exam, $token, $totalqsns, $totalCorrect, $totalWrong, $totalMarks, $duration, $passmarks, $pscore, $remarks);

    if($result){
        $res = array('status' => TRUE, 'data' => $token);
    }else {
        $res = array('status' => FALSE, 'msg' => 'Something Went Wrong!');
    }
    echo json_encode($res);


});

// Function to get results by token id
$app->post('/getResultsByTokenId', function ($request,$response) {
    $results = new results();
    $req = $request->getParsedBody();
    $results = $results->getUserResultsByToken($req['token']);

    if($results){
        $res = array('status' => TRUE, 'data' => $results);
    }else{
        $res = array('status' => FALSE, 'msg' => 'Something Went Wrong');
    }
    echo json_encode($res);

});

// Function to get results by token id
$app->post('/getSkillsByCourseId', function ($request,$response) {
    $results = new results();
    $req = $request->getParsedBody();
    $results = $results->getSkillsByCourseId($req['cid']);

    if($results){
        $res = array('status' => TRUE, 'data' => $results);
    }else{
        $res = array('status' => FALSE, 'msg' => 'Something Went Wrong');
    }
    echo json_encode($res);

});

// Function to get results by token id
$app->post('/getSkillsByUserId', function ($request,$response) {
    $results = new results();
    $req = $request->getParsedBody();
    $results = $results->getSkillsByUserId($req['uid']);

    if($results){
        $res = array('status' => TRUE, 'data' => $results);
    }else{
        $res = array('status' => FALSE, 'msg' => 'Something Went Wrong');
    }
    echo json_encode($res);

});

// Function to update skills
$app->post('/updateUserSkillsData', function ($request,$response) {
    $results = new results();
    $req = $request->getParsedBody();

    $results = $results->updateSkillsByCourseId($req);

    if($results){
        $res = array('status' => TRUE, 'msg' => 'Updated Succesfully!');
    }else{
        $res = array('status' => FALSE, 'msg' => 'Something Went Wrong');
    }
    echo json_encode($res);

});

$app->post('/createJob', function ($request,$response) {
    $jobs = new jobs();
    $req = $request->getParsedBody();

    //Check if job exists
    $exists =  $jobs->checkJobExists($req);

    if($exists){
        $results = $jobs->insertJob($req);
        
        if($results){
            $res = array('status' => TRUE, 'msg' => 'Created Succesfully!');
        }else{
            $res = array('status' => FALSE, 'msg' => 'Something Went Wrong');
        }
    }else {
        $res = array('status' => FALSE, 'msg' => 'Job Already Exists!');
    }
    echo json_encode($res);

});




//Function to get joblist
$app->post('/getJobsList', function ($request,$response) {
    $jobs = new jobs();

    $data = $jobs->getJobsList();

    if($data){
        $res = array('status' => TRUE, 'data' => $data);
    }else{
        $res = array('status' => FALSE, 'msg' => 'Something Went Wrong');
    }
    echo json_encode($res);
});


// Function to get job details by Id
$app->post('/getJobById', function ($request,$response) {
    $jobs = new jobs();

    $req = $request->getParsedBody();

    $data = $jobs->getJobsListById($req['jobid']);

    if($data){
        $res = array('status' => TRUE, 'data' => $data);
    }else{
        $res = array('status' => FALSE, 'msg' => 'Something Went Wrong');
    }
    echo json_encode($res);
});

//Function to get joblist
$app->post('/applyJobByUid', function ($request,$response) {
    $jobs = new jobs();

    $req = $request->getParsedBody();

    $data = $jobs->applyJobByUid($req);

    if($data){
        $res = array('status' => TRUE, 'msg' => 'Job Applied Successfully!');
    }else{
        $res = array('status' => FALSE, 'msg' => 'you have already applied!');
    }
    echo json_encode($res);

});

$app->post('/getAppliedJobs', function ($request,$response) {
    $jobs = new jobs();

    $data = $jobs->getAppliedJobs();

    if($data){
        $res = array('status' => TRUE, 'data' => $data);
    }else {
        $res = array('status' => FALSE, 'msg' => 'No Data Found!');
    }

    echo json_encode($res);

});




//Function to get all joblist
$app->post('/getAllJobsList', function ($request,$response) {
    $jobs = new jobs();

    $data = $jobs->getAllJobsList();

    if($data){
        $res = array('status' => TRUE, 'data' => $data);
    }else{
        $res = array('status' => FALSE, 'msg' => 'Something Went Wrong');
    }
    echo json_encode($res);
});

//Function to recover password
$app->post('/forgotPassword', function ($request,$response) {
    $auth = new auth();
    $req = $request->getParsedBody();
    $data = $auth->forgotPassword($req['email']);

    if($data && $data[0]->active == 'y'){
        $password = $auth->str_random(6);
        $subject = 'Empowering Talent Reset Password';
        $message = 'Here is your new password: '.$password;
        mail($data[0]->email,$subject,$message);

        $updatepwd = $auth->updatePassword($data[0]->email, $password);
        $res = array('status' => TRUE, 'msg' => 'password sent to your mail');

    }else if($data && $data[0]->active != 'y'){
        $res = array('status' => FALSE, 'msg' => 'Your accout has been disabled! please contact admin');
    }else {
        $res = array('status' => FALSE, 'msg' => 'You are not registered with us !');
    }
    echo json_encode($res);
});

//Function to check email exists
$app->post('/checkEmailExists', function ($request,$response) {
    $auth = new auth();
    $data = $auth->checkEmailExists($req['email']);

    if($data){
        // email exists
        return FALSE;
    }else {
        // emaill not exists
        return TRUE;
    }

});




$app->run();