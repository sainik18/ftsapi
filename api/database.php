<?php

class database {


        private $host = "localhost";
        private $user = "root";
        private $password = "";
        private $db_name = "examboard";


    // Connect
    public function connect(){
        $mysql_connect_str = "mysql:host=$this->host;dbname=$this->db_name";
        $dbConnection = new PDO($mysql_connect_str, $this->user, $this->password);
        $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //$dbConnection = new mysqli($this->host, $this->user, $this->password, $this->db_name);
        return $dbConnection;
    }


}