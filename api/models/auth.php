<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
include_once 'database.php';
require_once 'email.php';

class auth {

    public function checklogin ($email = NULL, $password = NULL) {

        $sql = "select * from admin_users where email = '$email' and password = '$password' and status = 'Y' ";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($data) {
            return $data;
        }else {
            return FALSE;
        }

    }

    public function checkuserlogin ($email = NULL, $password = NULL) {
        $encpwd = sha1($password);
        $sql = "select * from users where email = '$email' and password = '$encpwd' and active = 'y' ";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($data) {
            return $data;
        }else {
            return FALSE;
        }

    }

    public function getUserData($email = NULL) {
        
        if($email != NULL){
            $sql = "select * from users where email = '$email' and active = 'y' ";

            $db = new database();
            // Connect
            $db = $db->connect();
    
            $stmt = $db->query($sql);
    
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
    
            if($data) {
                return $data;
            }else {
                return FALSE;
            }
        }else {
            return FALSE;
        }
    }

    public function checkEmailExists ($email = NULL) {
        $sql = "select * from users where email = '$email' ";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($data) {
            return $data;
        }else {
            return FALSE;
        }

    }

    public function registerSocialUser ($email, $name, $image, $provider){
        $password = sha1(time());
        $username = explode("@",$email);

        $sql = "insert into users (username,email,password,fname,avatar,active) values ('$username[0]','$email','$password','$name','$image','y')";
        
        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        //$data = $stmt->fetchAll(PDO::FETCH_OBJ);

        $db = null;

        return TRUE;

    }

    //Function to register user
    public function registerUser($req){
        $pwd = $this->str_random(6);
        $password = sha1($pwd);
        $username = explode("@",$req['email']);

        $sql = "insert into users (fname,lname,username,email,password,phone,city,state,address,userlevel) values (:fname,:lname,:username,:email,:password,:phone,:city,:state,:address,:userlevel)";

        $db = new database();
        $db = $db->connect();
        $result = $db->prepare($sql);
        $exec = $result->execute(array(":fname" => $req['fname'], ":lname" => $req['lname'], ":username" => $username[0], ":email" => $req['email'], ":password" => $password,
        ":phone" => $req['phone'], ":city" => $req['city'], ":state" => $req['state'], ":address" => $req['address'], ':userlevel' => $req['userlevel'] ));

        $email = new email();
        $subject = 'Empowering Talent Registration Successful';
        $body = 'Hello '.$fname.', <br> Your New Password is :' .$pwd; 
        $email->sendmail($req['email'], $subject, $body);
        if($exec){
            return TRUE;
        }else {
            return FALSE;
        }
    }

    public function forgotPassword($email){
        $sql = "select email, active from users where email = '$email' ";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($data) {
            return $data;
        }else {
            return FALSE;
        }

    }

    // Function to Update user data
    public function updateUserData($data){
        $sql = "update users set fname = :fname, lname = :lname, phone = :phone, state = :state, zip = :zip, address = :address, avatar = :avatar where id = :id";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt= $db->prepare($sql);
        $stmt->execute(array(':fname' => $data['fname'], ':lname' => $data['lname'], ':phone' => $data['phone'], 
                            ':state' => $data['state'], ':zip' => $data['zip'], ':address' => $data['address'], ':avatar' => $data['avatar'], ':id' => $data['uid'] ));

        $db = null;
        return true;
    }

    // Function to update password
    public function updatePassword($email, $password){
        $pwd = sha1($password);

        $sql = "update users set password = :pwd where email = :email ";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt= $db->prepare($sql);
        $stmt->execute(array(':pwd' => $pwd, ':email' => $email));

        // $stmt = $db->query($sql);

        // $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        
        $db = null;

        return true;

    }

    // Function to get user education data
    public function getUserEduData($uid){
        $sql = "select * from education_info where user_id_fk = '$uid'";
        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($data) {
            return $data;
        }else {
            return FALSE;
        }
    }

    // Function to update user data
    public function updateUserEduData($data){
        $uid = $data['uid'];
        $sql = "select * from education_info where user_id_fk = '$uid' ";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt1 = $db->query($sql);

        $userdata = $stmt1->fetchAll(PDO::FETCH_OBJ);

        if($userdata){
            $sql = "update education_info set education = :education, board = :board, year_of_passing = :year_of_passing, medium = :medium, marks_percentage = :marks_percentage
                where user_id_fk = :user_id_fk";
        }else {
            $sql = "insert into education_info (user_id_fk, education, board, year_of_passing, medium, marks_percentage) values (:user_id_fk, :education, :board, :year_of_passing, :medium, :marks_percentage)";
        }

        
        //$db = new database();
        // Connect
        //$db = $db->connect();

        $stmt= $db->prepare($sql);
        $stmt->execute(array(':education' => $data['education'], ':board' => $data['board'], 
        ':year_of_passing' => $data['year_of_passing'], ':medium' => $data['medium'], 
        ':marks_percentage' => $data['marks_percentage'], ':user_id_fk' => $data['uid']));
        
        $db = null;

        return true;
    }

    //function to generate random password
    function str_random($length = 6){
    $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
}
}