<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
include_once 'database.php';

class jobs {

    public function getJobsList(){
        $sql = "select * from jobs_list where status = 'Y' order by created_at desc";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($data) {
            return $data;
        }else {
            return FALSE;
        }

    }

    public function getJobsListById($id){
        $sql = "select * from jobs_list where id = '$id' and status = 'Y' ";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($data) {
            return $data;
        }else {
            return FALSE;
        }

    }

    public function getAllJobsList(){
        $sql = "select * from jobs_list order by created_at desc";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($data) {
            return $data;
        }else {
            return FALSE;
        }

    }

    public function checkJobExists($req){
        $job_location = $req['job_location'];
        $companyName = $req['company_name'];
        $desingation = $req['desingation'];

        $sql = "select id from jobs_list where desingation = '$desingation' and company_name = '$companyName' and job_location = '$job_location' ";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($data) {
            return FALSE;
        }else {
            return TRUE;
        }
    }

    public function insertJob($req){

        $sql = "insert into jobs_list (job_type,desingation,company_name,company_img,job_description,job_link,min_qualification,
        job_location,last_date,experience,skills,salary) values (:job_type,:desingation,:company_name,:company_img,:job_description,
        :job_link,:min_qualification,:job_location,:last_date,:experience,:skills,:salary)";

        $db = new database();
        $db = $db->connect();
        $result = $db->prepare($sql);
        $exec = $result->execute(array(":job_type" => $req['job_type'], ":desingation" => $req['desingation'], ":company_name" => $req['company_name'],
        ":company_img" => $req['company_img'], ":job_description" => $req['job_description'], ":job_link" => $req['job_link'], 
        ":min_qualification" => $req['min_qualification'], ":job_location" => $req['job_location'], ":last_date" => $req['last_date'],
        ":experience" => $req['experience'], ":skills" => $req['skills'], ":salary" => $req['salary']));

        if($exec){
            return TRUE;
        }else {
            return FALSE;
        }
    }

    public function applyJobByUid($data){
        $job_id = $data['jobId'];
        $user_id = $data['uid'];

        $sql = "select * from user_applied_jobs where user_id_fk = '$user_id' and job_id_fk = '$job_id' ";
        $db = new database();
        $db = $db->connect();
        $stmt = $db->query($sql);

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($data) {
            return false;
        }else {
            $sql = "insert into user_applied_jobs (user_id_fk,job_id_fk) values ('$user_id','$job_id')";
            $db = new database();
            $db = $db->connect();
            $stmt = $db->query($sql);
            return true;
        }

    }

    public function getAppliedJobs(){
        // $sql = "select users.fname, users.lname,users.phone, jobs_list.company_name, jobs_list.desingation, user_applied_jobs.applied_date from users
        // INNER JOIN user_applied_jobs on user_applied_jobs.user_id_fk = users.id
        // INNER JOIN jobs_list on jobs_list.id = user_applied_jobs.job_id_fk";
        $sql = "select users.id, users.fname, users.lname,users.phone, users.email, education_info.education, education_info.board, education_info.year_of_passing, education_info.medium, education_info.marks_percentage, users.address, jobs_list.company_name, jobs_list.desingation, GROUP_CONCAT(DISTINCT(user_skills.skill_name)) as skills, user_applied_jobs.applied_date from users
        INNER JOIN user_applied_jobs on user_applied_jobs.user_id_fk = users.id
        LEFT JOIN education_info on education_info.user_id_fk = users.id
        LEFT JOIN user_skills on user_skills.user_id_fk = users.id
        INNER JOIN jobs_list on jobs_list.id = user_applied_jobs.job_id_fk GROUP BY id";

        $db = new database();
        $db = $db->connect();
        $stmt = $db->query($sql);

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($data) {
            return $data;
        }else {
            return false;
        }

    }
}