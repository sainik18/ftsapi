<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
include_once 'database.php';

class exams {

    public function getexamtypes () {

        //$sql = "select project_types.*, count(exams.id) as count from project_types, exams where exams.course = project_types.id ";
        $sql = "select project_types.*, count(exams.id) as examscount from project_types, exams where exams.course = project_types.id group by exams.course ";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($data) {
            return $data;
        }else {
            return FALSE;
        }

    }

    public function getexams ($courseid) {

        $sql = "select * from exams where course = :course and status = 'Y'";


        $db = new database();
        $db = $db->connect();
        $stmt = $db->prepare($sql);
        //$exec = $result->bindParam(array(":course" => $courseid));

        $stmt->bindParam(':course', $courseid, PDO::PARAM_INT); 
        $stmt->execute();

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        //$db = null;

        if($data) {
            return $data;
        }else {
            return FALSE;
        }

    }

    public function checkQuestionExists ($question) {

        $sql = "select * from questions where description = '$question' ";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($data) {
            return $data;
        }else {
            return FALSE;
        }

    }

    //Function to Insert Question
    public function insertQuestion ($data) {

        $exam = $data['exam'];
        $type = $data['optiontype'];
        $level = '1';
        $description = $data['question'];
        $marks = $data['marks'];
        $banned = '0';
        $qimage = $data['qimage'];

        $sql = "insert into questions (exam,type,level,description,qimage,marks,banned) values ('$exam','$type','$level','$description','$qimage','$marks','$banned') ";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        //$data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $id = $db->lastInsertId();

        $db = null;

        if($id) {
            return $id;
        }else {
            return FALSE;
        }

    }

    //Function insert answers by question id
    public function insertAnswersByQid($data, $qid){
        $option1 = $data['option1'];
        $option2 = $data['option2'];
        $option3 = $data['option3'];
        $option4 = $data['option4'];
        $banned = '0';

        if($data['correctAnswer'] == 1){
            $correct1 = 1;
        }else {
            $correct1 = 0;
        }
        if($data['correctAnswer'] == 2){
            $correct2 = 1;
        }else {
            $correct2 = 0;
        }
        if($data['correctAnswer'] == 3){
            $correct3 = 1;
        }else {
            $correct3 = 0;
        }
        if($data['correctAnswer'] == 4){
            $correct4 = 1;
        }else {
            $correct4 = 0;
        }
        $sql1 = " insert into answers (question,answer,correct,banned) values ('$qid','$option1','$correct1','$banned')";
        $sql2 = " insert into answers (question,answer,correct,banned) values ('$qid','$option2','$correct2','$banned')";
        $sql3 = " insert into answers (question,answer,correct,banned) values ('$qid','$option3','$correct3','$banned')";
        $sql4 = " insert into answers (question,answer,correct,banned) values ('$qid','$option4','$correct4','$banned')";
        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql1);
        $stmt = $db->query($sql2);
        $stmt = $db->query($sql3);
        $stmt = $db->query($sql4);

        //$data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $id = $db->lastInsertId();

        return TRUE;
        

    }

    // Function to get exam id
    public function getExamId($eid){
        $sql = " select * from exams where examid = '$eid' ";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($data) {
            return $data;
        }else {
            return FALSE;
        }

    }

    // Function to get questions by Exam id
    public function getAllQuestionsByExamId($id){
        $sql = "select * from questions where exam = '$id' and banned = 0";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($data) {
            return $data;
        }else {
            return FALSE;
        }
    }

    // Function to get questions by Exam id
    public function getQuestionsByExamId($id){
        $sql = "select * from questions where exam = '$id' and banned = 0 limit 30";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($data) {
            return $data;
        }else {
            return FALSE;
        }
    }

    // Function to get answers by Question id
    public function getAnswersbyQid($qid){

        $sql = " select id,answer,correct from answers where question = '$qid' and banned = 0 ";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($data) {
            return $data;
        }else {
            return FALSE;
        }

    }

    //Function to get question by Qid
    public function getQuestionByQid($qid){

        $sql = "select id, exam, type, description, marks from questions where id = '$qid' ";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($data) {
            return $data;
        }else {
            return FALSE;
        }
    }

    //Function to insert tquestion

    public function insertTQuestion($qdata, $uid, $token){
        //$sql = " insert into 'tquestions' ('uid','token','qid','type','exam','description','marks') values ('$uid','$token','$qdata->id','$qdata->type','$qdata->exam','$qdata->description','$qdata->marks') ";
        $sql = "insert into tquestions (uid,token,qid,type,exam,description,marks) values (:uid,:token,:id,:type,:exam,:description,:marks)";
        
        $db = new database();
        $db = $db->connect();
        $result = $db->prepare($sql);
        $exec = $result->execute(array(":uid" => $uid, ":token" => $token, ":id" => $qdata->id, ":type" => $qdata->type, ":exam" => $qdata->exam, ":description" => $qdata->description, ":marks" => $qdata->marks ));
        if($exec){
            return $db->lastInsertId();
        }else {
            return "not inserted";
        }
        
    }

    //Function to get answer by id
    public function getAnswerbyId($qid){
        $sql = " select id, question, answer, correct from answers where question = '$qid' and banned = '0' ";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($data) {
            return $data;
        }else {
            return FALSE;
        }
    }

    //Function to insert tanswer
    public function insertTAnswer($adata, $uid, $qid, $correct, $marked){
        $answer = $adata->answer;
        //$sql = " insert into tanswers (tquestion,uid,answer,correct,marked) values ('$qid','$uid','$answer','$correct','$marked') ";
        $sql = " insert into tanswers (tquestion,uid,answer,correct,marked) values (:qid,:uid,:answer,:correct,:marked) ";

        $db = new database();
        // Connect
        $db = $db->connect();

        $result = $db->prepare($sql);
        $exec = $result->execute(array(":qid" => $qid, ":uid" => $uid, ":answer" => $answer, ":correct" => $correct, ":marked" => $marked));

        $data = $db->lastInsertId();
        $db = null;
        
        return $data;

    }

    public function editQuestionAndAnswers($req){
        $eid = $req['eid'];
        $question = $req['question'];
        $qimage = $req['qimage'];
        $optionid1 = $req['optionId1'];
        $optionid2 = $req['optionId2'];
        $optionid3 = $req['optionId3'];
        $optionid4 = $req['optionId4'];
        $option1 = $req['option1'];
        $option2 = $req['option2'];
        $option3 = $req['option3'];
        $option4 = $req['option4'];
        $correct = $req['correctAnswer'];
        $correct1 = 0;
        $correct2 = 0;
        $correct3 = 0;
        $correct4 = 0;

        if($correct == 1){
            $correct1 = 1;
        }
        if($correct == 2){
            $correct2 = 1;
        }
        if($correct == 3){
            $correct3 = 1;
        }
        if($correct == 4){
            $correct4 = 1;
        }

        //Update question
        $sql = "update questions set description = :description, qimage = :qimage where id = '$eid'";

        $db = new database();
        // Connect
        $db = $db->connect();
        $result = $db->prepare($sql);
        $exec = $result->execute(array(":description" => $question,":qimage" => $qimage));

        $data = $db->lastInsertId();
        $db = null;

        //Update option 1
        $sql1 = "update answers set answer = :answer1, correct = :correct1 where id = '$optionid1'";
        $db = new database();
        // Connect
        $db = $db->connect();
        $result = $db->prepare($sql1);
        $exec = $result->execute(array(":answer1" => $option1, ':correct1' => $correct1));

        $data = $db->lastInsertId();
        $db = null;

        //Update option 2
        $sql2 = "update answers set answer = :answer2, correct = :correct2 where id = '$optionid2'";
        $db = new database();
        // Connect
        $db = $db->connect();
        $result = $db->prepare($sql2);
        $exec = $result->execute(array(":answer2" => $option2, ':correct2' => $correct2));

        $data = $db->lastInsertId();
        $db = null;

        //Update option 3
        $sql3 = "update answers set answer = :answer3, correct = :correct3 where id = '$optionid3'";
        $db = new database();
        // Connect
        $db = $db->connect();
        $result = $db->prepare($sql3);
        $exec = $result->execute(array(":answer3" => $option3, ':correct3' => $correct3));

        $data = $db->lastInsertId();
        $db = null;

        //Update option 4
        $sql4 = "update answers set answer = :answer4, correct = :correct4 where id = '$optionid4'";
        $db = new database();
        // Connect
        $db = $db->connect();
        $result = $db->prepare($sql4);
        $exec = $result->execute(array(":answer4" => $option4, ':correct4' => $correct4));

        $data = $db->lastInsertId();
        $db = null;
        
        return TRUE;

    }

    //Function to save user report
    public function saveUserResult ($uid, $exam, $token, $totalqsns, $totalCorrect, $totalWrong, $fullmarks, $duration, $passmarks, $pscore, $remarks){
        $date = date('Y-m-d');
        $sql = " insert into results (user,exam,token,total_questions,correct_answers,wrong_answers,fullmarks,duration,marks,score,remarks,date) values 
        ('$uid','$exam','$token','$totalqsns','$totalCorrect','$totalWrong','$fullmarks','$duration','$passmarks','$pscore','$remarks','$date')";

        // $sql = " insert into results (user,exam,token,total_questions,correct_answers,wrong_answers,fullmarks,duration,marks,score,remarks,date) values 
        // (:uid,:exam,:token,:totalqsns,:totalCorrect,:totalWrong,:fullmarks,:duration,:passmarks,:pscore,:remarks,:date)";

        $db = new database();
        // Connect
        $db = $db->connect();

        //$exec = $result->execute(array(":uid" => $uid,":exam" => $exam,":token" => $token,":totalqsns" => $totalqsns,":totalCorrect" => $totalCorrect,":totalWrong" => $totalWrong,":fullmarks" => $fullmarks,":duration" => $duration,":passmarks" => $passmarks,":pscore" => $pscore,":remarks" => $remarks,":date" => $date));
        $stmt = $db->query($sql);

        $data = $db->lastInsertId();
        $db = null;
        
        return $data;
    }


}