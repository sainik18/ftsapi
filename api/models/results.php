<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
include_once 'database.php';

class results {

    public function getUserResults($userid) {

        $sql = "select results.duration, results.pass_marks, results.marks, results.fullmarks, results.score, results.remarks, results.date,
        exams.title, exams.id, exams.examid as examid from results, exams where results.user = '$userid' and results.exam = exams.id order by results.date DESC ";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($data) {
            return $data;
        }else {
            return FALSE;
        }
    }

    public function getUserResultsByToken($token){
        $sql = " select * from results where token = '$token' ";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($data) {
            return $data;
        }else {
            return FALSE;
        }
    }

    public function getReviewExamByTokenId($uid, $token){
        $sql = "select id,qid,description from tquestions where token = '$token' and uid = '$uid' ";

        $db = new database();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $tqsns = $stmt->fetchAll(PDO::FETCH_OBJ);

        $db = null;
        if($tqsns){
            $tqids = '';
            foreach($tqsns as $tqn){
                $tqids .= $tqn->id.",";
            }
        }
        $tqids = rtrim($tqids,",");

        $sql1 = "select tanswers.*, tquestions.description as question, tquestions.qid from tanswers, tquestions where tquestion in ($tqids) and tquestions.id = tanswers.tquestion";

        $db = new database();
        // Connect
        $db = $db->connect();
        $stmt1 = $db->query($sql1);

        $tans = $stmt1->fetchAll(PDO::FETCH_OBJ);

        $db = null;
        if($tans) {
            return $tans;
        }else {
            return FALSE;
        }

    }

    public function getSkillsByCourseId($cid){
        $sql = "select * from skills_list where project_type_id = '$cid' and status = 'Y' order by skill_name ASC";
        
        $db = new database();
        // Connect
        $db = $db->connect();
        $stmt1 = $db->query($sql);

        $tans = $stmt1->fetchAll(PDO::FETCH_OBJ);

        $db = null;
        if($tans) {
            return $tans;
        }else {
            return FALSE;
        }
    }

    public function getSkillsByUserId($uid){
        $sql = "select * from user_skills where user_id_fk = '$uid' and status = 'Y'";
        $db = new database();
        // Connect
        $db = $db->connect();
        $stmt1 = $db->query($sql);
        $resp = $stmt1->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        if($resp) {
            return $resp;
        }else {
            return FALSE;
        }
    }

    public function updateSkillsByCourseId($data){
        $skillsArray = $data['skillsArray'];
        $uid = $data['uid'];

        foreach($skillsArray as $skillid){
            $sql = "select * from user_skills where skill_id_fk = '$skillid' and user_id_fk = '$uid' ";
            $db = new database();
            // Connect
            $db = $db->connect();
            $stmt = $db->query($sql);

            $uskills = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;

            if(empty($uskills)){
                $sql1 = "select * from skills_list where id = '$skillid' ";

                $db = new database();
                // Connect
                $db = $db->connect();
                $stmt1 = $db->query($sql1);
                $skillName = $stmt1->fetchAll(PDO::FETCH_OBJ);
                $db = null;

                $sname = $skillName[0]->skill_name;
                $sql2 = "insert into user_skills (user_id_fk,skill_id_fk,skill_name) values (:uid,:skillid,:sname)";
                $db = new database();
                // Connect
                $db = $db->connect();
                $result = $db->prepare($sql2);
                $exec = $result->execute(array(":uid" => $uid, ":skillid" => $skillid, ":sname" => $sname));
                $db = null;

            }
        }
        return true;
        
    }
}